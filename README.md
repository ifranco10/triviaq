# TriviaQ

This project is part of an exercise for a fintech company.

## Goal

The project has like goal render a list of *Trivia Questions*. Like programming language I used Javascript, and React like library. I have also used axios to fetching API data, and styled-component, Material UI and react-bootstrap to quickly layout components.

For test components, I have used Enzyme to mock components. I test only one component that retrieve a question data.

I have used index.js fyle in all components because is possible if I wanted to use a redux, or recoil config envolving the component in this fyle. Besides, **this method facilitates import components** in another fyles.

I have created a **config-data.js emulating a .env fyle**, this .env will store the API url, API Key, or connection to BD and another information. In the API.js file I've created a method like dessing pattern Object literals. 

Also I have used a **eslint and prettier** to guarantee as much as possible the normalization of code in a possible team work.

### Next steps

I would like to implement a real-time filtering functionality in questions with a searchbar in table. I would use a filter array function of Javascript, and render the new array result.

Also, if the API returned correctly the ID's, I would create an ordered system using sort() method Javascript. 

In the component see answer, I would change the state of button, when the button is clicked, change the color or icon, even, I would use a switch button. 

I have paginated only the 50 results of the request, but is possible paginate passing to URL the number of page, so the API returns the result paginated and you cand limit the result. 

I have created a method (unused) to return all categories, for, for example, render a buttons with all categories y when are clicked, render a list filtered whit this categories, called to API whit this data. 

Another goal pending, is handle errors when is impossible return data from API. 

Is possible to use *husky* or another code-checked before deploy in production enviroment.

# Init project

I'll leave the project in develop environment, but, if You desire to deploy in production environment, You must compile project with **npm run build** in master.

```
cd client
npm install
npm start
```