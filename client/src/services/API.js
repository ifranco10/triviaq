import axios from "axios";
import config from "./config/config-data";

const URL = config.apiURL;

const api = {
  getAllQuestions: async () => {
    try {
      const questions = await axios.get(`${URL}/api.php?amount=50`);
      return questions.data.results;
    } catch (err) {
      alert("Oops! Don't get all questions, Try again");
    }
  },

  getAllCategories: async () => {
    try {
      const questions = await axios.get(`${URL}/api_category.php`);
      return questions.data.results;
    } catch (err) {
      alert("Oops! Don't get categories, Try again");
    }
  }
};

export default api;
