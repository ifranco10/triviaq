import React from "react";
import "./Pagination.css";
import { Pagination } from "react-bootstrap";



const paginationBasic = ({ questPerPage, totalQuest, paginate }) => {

    let items = [];
    for (let number = 1; number <= Math.ceil(totalQuest / questPerPage); number++) {
      items.push(
        <Pagination.Item key={number} className="paginate-item" onClick={() => paginate(number)}>
          {number}
        </Pagination.Item>,
      );
    }

  return (
    <div className="d-flex justify-content-center">
      <Pagination size="lg">{items}</Pagination>
    </div>
  );
};

export default paginationBasic;
