import React from "react";
import { Nav, Navbar } from "react-bootstrap";
import styled from "styled-components";

/*icons Material UI*/
import MenuIcon from "@material-ui/icons/Menu";
import AddIcon from "@material-ui/icons/Add";
import SettingsIcon from "@material-ui/icons/Settings";
import ForumIcon from "@material-ui/icons/Forum";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const TitleNav = styled.h2`
  font-size: 1.5em;
  color: #fff;
`;

const NavContainer = styled.section`
  background-color: #262a35;
  margin-bottom: 2em;
`;

const NavLinkA = styled.a`
  display: flex;
  font-size: 13px;
  color: #fff;
  text-transform: uppercase;
  padding: 5px 15px;
  background: rgba(0, 0, 0, 0.6);
  margin-right: 10px;
  transition: all 0.35s;

  &:hover {
    text-decoration: none;
    color: #fff;
    background-color: #5c6378;
  }

  & > svg{
    font-size: 21px;
    margin-right: 5px;
  }
`;

export default function NavBar() {
  return (
    <NavContainer>
      <Navbar collapseOnSelect expand="lg">
        <Navbar.Brand href="#home">
          <TitleNav>TriviaQ</TitleNav>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ml-auto">
            <NavLinkA href="#0">
              <MenuIcon />
              browse
            </NavLinkA>
            <NavLinkA href="#0"><AddIcon />add new question</NavLinkA>
            <NavLinkA href="#0"><SettingsIcon />api</NavLinkA>
            <NavLinkA href="#0"><ForumIcon />discuss</NavLinkA>
            <NavLinkA href="#0"><ExitToAppIcon />login</NavLinkA>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </NavContainer>
  );
}
