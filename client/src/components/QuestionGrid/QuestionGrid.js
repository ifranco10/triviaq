import React, { useState, useEffect } from "react";
import api from "../../services/API";
import QuestionList from "../QuestionList";
import Pagination from "../Pagination";
import styled from "styled-components";

import { Row, Col, Table } from "react-bootstrap";

const TextComponent = styled.h2`
  text-align: left;
`;

const TableStyled = styled(Table)`
  color: #fff;
  text-transform: capitalize;
  background: #383e4e;
  box-shadow: 16px 16px 31px #323745, -16px -16px 31px #3e4557;

  & thead {
    background-color: #2c2c2c;

    & tr > th {
      border: 1px solid #464545;
    }
  }

  & tbody > tr {
    background-color: #1b1b1b;
  }

  & tbody > tr > td {
    border: 1px solid #464545;
  }
`;

export default function QuestionGrid() {
  const [questions, setQuestions] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [questPerPage] = useState(10);

  useEffect(() => {
    const fetchData = async () => {
      const res = await api.getAllQuestions();
      setQuestions(res);
    };

    fetchData();
  }, []);

  const indexOfLastQuestion = currentPage * questPerPage;
  const indexOfFirstQuestion = indexOfLastQuestion - questPerPage;
  const currentQuestion = questions.slice(
    indexOfFirstQuestion,
    indexOfLastQuestion,
  );

  const paginate = pageNumber => setCurrentPage(pageNumber)

  if (!questions) return <div>Loading...</div>;

  return (
    <>
      <Row>
        <Col>
          <TextComponent>Browse Questions</TextComponent>
        </Col>
      </Row>
      <Row>
        <Col>
          <TableStyled bordered className="text-left">
            <thead>
              <tr>
                <th>Category</th>
                <th>Type</th>
                <th>Difficulty</th>
                <th>Question / Statement</th>
                <th>See answer</th>
              </tr>
            </thead>
            <QuestionList questions={currentQuestion} />
          </TableStyled>
        </Col>
      </Row>
      <Row>
        <Col>
          <Pagination
            questPerPage={questPerPage}
            totalQuest={questions.length}
            paginate={paginate}
          />
        </Col>
      </Row>
    </>
  );
}


