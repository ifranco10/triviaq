import React from "react";
import Popover from "../Popover";

function decodeHtml(html) {
  var txt = document.createElement("textarea");
  txt.innerHTML = html;
  return txt.value;
}

const renderQuestions = ({ questions }) => {

  return (
    <tbody>
      {questions.map((q) => (
        <tr key={q.question}>
          <td>{` ${q.category} `}</td>
          <td>{` ${q.type === "boolean" ? "true/false" : q.type} `}</td>
          <td>{` ${q.difficulty} `}</td>
          <td>{` ${decodeHtml(q.question)} `}</td>
          <td><Popover answer={`${q.correct_answer}`} /></td>
        </tr>
      ))}
    </tbody>
  );
};

export default renderQuestions;