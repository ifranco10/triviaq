import React from "react";
import { shallow } from "enzyme";
import QuestionList from "./QuestionList";

describe("Question List", () => {

    const mockQuestion = [
      {
        category: "Entertainment: Video Games",
        type: "multiple",
        difficulty: "medium",
        question: "What was the character Kirby originally going to be named?",
        correct_answer: "Popopo",
      },
    ];

    test("renders", () => {

        const wrapper = shallow(<QuestionList questions={mockQuestion}/>);
        expect(wrapper.exists()).toBe(true);
    });

});
