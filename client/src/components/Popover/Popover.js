import React from "react";
import { Popover, Button, OverlayTrigger } from "react-bootstrap";
import VisibilityIcon from "@material-ui/icons/Visibility";

const PopoverItem = ({answer}) => {

    return (
      <OverlayTrigger
        trigger="click"
        placement="bottom"
        overlay={
          <Popover id="popover-positioned-bottom">
            <Popover.Content>
              {answer}
            </Popover.Content>
          </Popover>
        }
      >
        <Button variant="secondary"><VisibilityIcon /></Button>
      </OverlayTrigger>
    );
}

export default PopoverItem;
