import React from 'react';
import './App.css';
import QuestionGrid from "./components/QuestionGrid";
import { Container, Row, Col } from "react-bootstrap";

function App() {
  return (
    <div className="App">
      <Container>
        <Row>
          <Col>
            <QuestionGrid />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
